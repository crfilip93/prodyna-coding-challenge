import {
  NgModule
} from '@angular/core';

import {
  PostsRoutingModule,
  routedComponents
} from './posts-routing.module';
import {
  SharedModule
} from '../shared/shared.module';

@NgModule({
  imports: [
    PostsRoutingModule,
    SharedModule
  ],
  declarations: [routedComponents]
})
export class PostsModule {}