import {
  Component,
  OnInit,
  OnDestroy,
  PLATFORM_ID,
  Inject,
  HostListener
} from '@angular/core';
import {
  TransferState,
  makeStateKey
} from '@angular/platform-browser';
import {
  isPlatformBrowser,
  isPlatformServer
} from '@angular/common';
import {
  DOCUMENT
} from '@angular/platform-browser';
import {
  fromEvent
} from 'rxjs/observable/fromEvent';
import {
  distinct,
  filter,
  map,
  debounceTime,
  tap,
  flatMap
} from 'rxjs/operators';
import {
  Observable
} from 'rxjs/Observable';
import {
  Subscription
} from 'rxjs';

import {
  Post,
  PostService
} from '../models/';
import {
  WINDOW,
  ModalService,
  ToastService
} from "../core/";
import {
  FilterTextService
} from '../shared/filter-text/filter-text.service';
import {
  Animations
} from '../core/animations';

const POSTS_KEY = makeStateKey('posts');


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  animations: [Animations]
})
export class PostsComponent implements OnInit, OnDestroy {

  posts: Post[] = [];
  filteredPosts = this.posts;
  upperLimit: number = 10;
  showVariable: number = -1;
  searchText: string = '';
  infiniteScroll: Subscription;


  constructor(
    private state: TransferState,
    private postService: PostService,
    private filterService: FilterTextService,
    private modalService: ModalService,
    private toastService: ToastService,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  ngOnInit() {

    if (isPlatformBrowser(this.platformId)) {

      this.posts = this.filteredPosts = this.state.get(POSTS_KEY, null as any);

      this.infiniteScroll = this.getInfScrollObservable().subscribe(
        () => {
          this.upperLimit > this.posts.length ? this.upperLimit = this.posts.length : this.upperLimit += 10;
        }
      )

    } else if (isPlatformServer(this.platformId)) {

      this.getPosts();

    }
    /**
     * for 'npm serve build' uncomment fun() below
     */
    this.getPosts();


  }

  ngOnDestroy() {
    this.infiniteScroll.unsubscribe();
  }

  filterChanged(searchText: string) {
    this.filteredPosts = this.filterService.filter(
      searchText, ['title', 'body'],
      this.posts
    );
    this.upperLimit = 10;
    this.searchText = searchText;
  }
  /**
   * @HostListener load on scroll solution
   */
  // @HostListener("window:scroll", ['$event'])
  // onWindowScroll() {

  //   let scrollFromTop = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;

  //   let documentHeight = Math.max(this.document.body.scrollHeight, this.document.body.offsetHeight,
  //     this.document.documentElement.clientHeight, this.document.documentElement.scrollHeight, this.document.documentElement.offsetHeight);

  //   let viewPortHeight = this.window.innerHeight

  //   if (scrollFromTop + viewPortHeight > documentHeight - 500) {
  //     this.upperLimit > this.posts.length ? this.upperLimit = this.posts.length : this.upperLimit += 10;
  //   }
  // }


  /**
   * Observable load on scroll solution
   */
  getInfScrollObservable: () => Observable < any > = () => {
    return fromEvent(this.window, "scroll")
      .pipe(
        map(() => {
          let viewPortHeight = this.window.innerHeight
          let documentHeight = Math.max(this.document.body.scrollHeight, this.document.body.offsetHeight,
            this.document.documentElement.clientHeight, this.document.documentElement.scrollHeight, this.document.documentElement.offsetHeight);

          let scrollFromTop = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;

          return scrollFromTop + viewPortHeight > documentHeight - 500;
        }),
        filter(result => result == true),
        debounceTime(10)
        // distinct()
      );
  }

  getPosts() {
    this.posts = this.filteredPosts = [];
    this.showVariable = -1;

    this.postService.getPosts().subscribe(posts => {
        this.posts = this.filteredPosts = posts;
        this.filterChanged(this.searchText);
        this.state.set(POSTS_KEY, posts as any);
      },
      () => this.toastService.activate('Error fetching posts')
    );
  }


  deletePost(post) {
    let msg = `Are you sure you want to delete this post?`;
    this.modalService.activate(msg).then(responseOK => {
      if (responseOK) {
        this.postService.deletePost(post).subscribe(
          () => {
            this.toastService.activate('Successfully deleted')
            this.getPosts()
          },
          () => this.toastService.activate('Error deleting post')
        );
      }
    });
  }

}