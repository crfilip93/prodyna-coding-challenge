import {
  NgModule
} from '@angular/core';
import {
  PreloadAllModules,
  Routes,
  RouterModule
} from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'posts',
  },

  {
    path: 'posts',
    loadChildren: 'app/posts/posts.module#PostsModule'
  },
  {
    path: 'add-new-post',
    loadChildren: 'app/add-new-post/add-new-post.module#AddNewPostModule'
  },

  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'posts'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    initialNavigation: 'enabled'
  })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}