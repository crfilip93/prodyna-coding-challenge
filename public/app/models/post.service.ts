import {
  HttpClient
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import {
  catchError,
  finalize,
  map
} from 'rxjs/operators';

import {
  CONFIG,
  ExceptionService,
  SpinnerService
} from '../core';
import {
  Post
} from './post.model';

let apiUrl = CONFIG.apiUrl;

@Injectable()
export class PostService {

  constructor(
    private http: HttpClient,
    private exceptionService: ExceptionService,
    private spinnerService: SpinnerService
  ) {}

  addPost(post: Post) {
    return <Observable < Post >> (
      this.http.post(`${apiUrl}`, post).pipe(
        map((res: any) => < Post > res),
        catchError(this.exceptionService.catchBadResponse),
      )
    );
  }

  deletePost(post: Post) {
    return <Observable < Post >> (
      this.http.delete(`${apiUrl}/${post.id}`).pipe(
        map(res => this.extractData < Post > (res)),
        catchError(this.exceptionService.catchBadResponse),
      )
    );
  }

  getPosts() {
    return <Observable < Post[] >> this.http.get(apiUrl).pipe(
      map(res => this.extractData < Post[] > (res)),
      catchError(this.exceptionService.catchBadResponse),
    );
  }



  private extractData < T > (res: any) {
    if (res && (res.status < 200 || res.status >= 300)) {
      throw new Error('Bad response status: ' + res.status);
    }
    return <T > (res || {});
  }
}