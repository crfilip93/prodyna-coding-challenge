import {
  NgModule,
  Optional,
  SkipSelf
} from '@angular/core';
import {
  CommonModule
} from '@angular/common';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  RouterModule
} from '@angular/router';
import {
  BrowserAnimationsModule
} from '@angular/platform-browser/animations';
import {
  HttpModule
} from '@angular/http';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerInterceptor } from './http.interceptor';

import {
  NavbarComponent
} from './navbar/navbar.component';
import {
  FooterComponent
} from './footer/footer.component';
import {
  throwIfAlreadyLoaded
} from './module-import-guard';
import {
  SpinnerModule
} from './spinner/spinner.module';
import {
   ToastModule
} from './toast/toast.module';
import {
  ModalModule
} from './modal/modal.module';
import {
  WINDOW_PROVIDERS
} from "./window.service";
import {
  ExceptionService
} from './exception.service';

@NgModule({
  imports: [
    CommonModule, FormsModule, RouterModule, ReactiveFormsModule, BrowserAnimationsModule, HttpModule, SpinnerModule,ToastModule, ModalModule
  ],
  providers: [ExceptionService, WINDOW_PROVIDERS,
   {
    provide: HTTP_INTERCEPTORS,
    useClass: SpinnerInterceptor,
    multi: true
  }],
  declarations: [NavbarComponent, FooterComponent],
  exports: [CommonModule, FormsModule, RouterModule, BrowserAnimationsModule, ReactiveFormsModule, HttpModule, SpinnerModule,ToastModule, ModalModule, [NavbarComponent, FooterComponent]],
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule
  ) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}