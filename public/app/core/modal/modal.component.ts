import {
  Component,
  OnInit,
  Inject
} from '@angular/core';

import {
  ModalService
} from './modal.service';

import {
  DOCUMENT
} from '@angular/platform-browser';

import {
  WINDOW,
} from "../window.service";


const KEY_ESC = 27;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  title: string;
  message: string;
  okText: string;
  cancelText: string;
  negativeOnClick: (e: any) => void;
  positiveOnClick: (e: any) => void;

  private defaults = {
    title: 'Confirmation',
    message: 'Some default message',
    cancelText: 'Cancel',
    okText: 'OK'
  };
  private modalElement: any;
  private cancelButton: any;
  private okButton: any;

  constructor(modalService: ModalService, @Inject(DOCUMENT) private document: Document, @Inject(WINDOW) private window) {
    modalService.activate = this.activate.bind(this);
  }

  activate(message = this.defaults.message, title = this.defaults.title) {
    this.title = title;
    this.message = message;
    this.okText = this.defaults.okText;
    this.cancelText = this.defaults.cancelText;

    let promise = new Promise < boolean > ((resolve, reject) => {
      this.negativeOnClick = (e: any) => resolve(false);
      this.positiveOnClick = (e: any) => resolve(true);
      this.show();
    });

    return promise;
  }

  ngOnInit() {
    this.modalElement = this.document.getElementById('confirmationModal');
    this.cancelButton = this.document.getElementById('cancelButton');
    this.okButton = this.document.getElementById('okButton');
  }

  private show() {
    this.document.onkeyup = null;

    if (!this.modalElement || !this.cancelButton || !this.okButton) {
      return;
    }

    this.modalElement.style.opacity = 0;
    this.modalElement.style.zIndex = 9999;

    this.cancelButton.onclick = (e: any) => {
      e.preventDefault();
      if (!this.negativeOnClick(e)) {
        this.hideModal();
      }
    };

    this.okButton.onclick = (e: any) => {
      e.preventDefault();
      if (!this.positiveOnClick(e)) {
        this.hideModal();
      }
    };

    this.modalElement.onclick = () => {
      this.hideModal();
      return this.negativeOnClick(null);
    };

    this.document.onkeyup = (e: any) => {
      if (e.which === KEY_ESC) {
        this.hideModal();
        return this.negativeOnClick(null);
      }
    };

    this.modalElement.style.opacity = 1;
  }

  private hideModal() {
    this.document.onkeyup = null;
    this.modalElement.style.opacity = 0;
    this.window.setTimeout(() => (this.modalElement.style.zIndex = -1), 300);
  }
}