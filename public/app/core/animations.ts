import {
    trigger,
    animate,
    style,
    group,
    transition,
    state
} from '@angular/animations';



const slideUp = [
    trigger('slideUp', [
        state('in', style({
            transform: 'translateY(0)',
            opacity: 1
        })),
        transition('void => *', [
            style({
                transform: 'translateY(30px)',
                opacity: 0
            }),
            group([
                animate('0.3s 0.1s ease', style({
                    transform: 'translateY(0px)',
                })),
                animate('0.3s ease', style({
                    opacity: 1
                }))
            ])
        ]),
        transition('* => void', [
            group([
                animate('0.3s 0.2s ease', style({
                    opacity: 0
                }))
            ])
        ])
    ])
]

const slideRight = [
    trigger('slideRight', [
        state('in', style({
            transform: 'translateX(-0%)',
            opacity: 1
        })),
        transition('void => *', [
            style({
                transform: 'translateX(-2%)',
                opacity: 0
            }),
            animate(150)
        ]),
        transition('* => void', [
            animate(200, style({
                opacity: 0
            }))
        ])
    ])
]

export const Animations = [slideUp, slideRight]