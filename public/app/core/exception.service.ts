import {
  HttpErrorResponse
} from '@angular/common/http';
import {
  Injectable
} from '@angular/core';
import {
  Observable
} from 'rxjs';
import { 
  of
} from 'rxjs/observable/of';


@Injectable()
export class ExceptionService {
  
  constructor() {}

  catchBadResponse: (errorResponse: any) => Observable < any > = (
    errorResponse: any
  ) => {
    let res = < HttpErrorResponse > errorResponse;
    let err = res;
    let emsg = err ?
      err.error ?
      err.error :
      JSON.stringify(err) :
      res.statusText || 'unknown error';
    console.log(emsg);
    return of(false);
  };
}