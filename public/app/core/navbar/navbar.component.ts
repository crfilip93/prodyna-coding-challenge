import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';

class MenuItem {
  constructor(public caption: string, public link: any[]) {}
}

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menuItems: MenuItem[];
  currentRoute;

  constructor(public router: Router) {}

  ngOnInit() {
    this.menuItems = [{
        caption: 'Prodyna Coding Challenge',
        link: ['/posts']
      },
      {
        caption: 'Add New Post',
        link: ['/add-new-post']
      },
    ];

    this.router.events.subscribe(() => {
      this.currentRoute = '/' + this.router.url.split('/')[1];
    });
  }

}