import {
  BrowserModule,
  BrowserTransferStateModule
} from '@angular/platform-browser';
import {
  NgModule
} from '@angular/core';
import {
  TransferHttpCacheModule
} from '@nguniversal/common';
import {
  HttpClientModule
} from '@angular/common/http';

import {
  AppRoutingModule
} from './app-routing.module';
import {
  CoreModule
} from './core/core.module';
import {
  AppComponent
} from './app.component';
import {
  PostService
} from './models/post.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({
      appId: 'my-app'
    }),
    BrowserTransferStateModule,
    TransferHttpCacheModule,
    HttpClientModule,
    CoreModule,
    AppRoutingModule
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule {}