import {
  CommonModule
} from '@angular/common';
import {
  NgModule
} from '@angular/core';
import { 
  ReactiveFormsModule
} from '@angular/forms';

import {
  FilterTextModule
} from './filter-text/filter-text.module';
import {
  InitCapsPipe
} from './init-caps.pipe';

@NgModule({
  imports: [CommonModule, FilterTextModule, ReactiveFormsModule],
  exports: [CommonModule, FilterTextModule, ReactiveFormsModule, InitCapsPipe],
  declarations: [InitCapsPipe]
})
export class SharedModule {}