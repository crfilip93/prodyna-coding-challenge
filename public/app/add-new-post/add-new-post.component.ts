import {
    Component,
    OnInit
} from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators
} from '@angular/forms';
import {
    Router
} from '@angular/router';

import {
    PostService
} from '../models/';
import{
    ToastService
} from '../core';
import {
    Animations
} from '../core/animations';

@Component({
    selector: 'app-add-new-post',
    templateUrl: './add-new-post.component.html',
    styleUrls: ['./add-new-post.component.css'],
    animations: [Animations]
})
export class AddNewPostComponent implements OnInit {

    private postData = {
        title: '',
        body: ''
    }

    postForm: FormGroup;

    constructor(private fb: FormBuilder, private postService: PostService, private toastService: ToastService, private router: Router) {}

    ngOnInit() {

        this.buildForm();
    }

    buildForm(): void {
        this.postForm = this.fb.group({
            'title': [this.postData.title, [
                Validators.required
            ]],
            'body': [this.postData.body, [
                Validators.required,
            ]],
        });

        this.postForm.valueChanges
            .subscribe(data => this.onValueChanged(data));

        this.onValueChanged();
    }

    formErrors = {
        'title': '',
        'body': '',
    };

    validationMessages = {
        'title': {
            'required': 'Title is required!',
        },
        'body': {
            'required': 'Content is required!',
        }
    };

    onValueChanged(data ? : any) {
        if (!this.postForm) {
            return;
        }
        const form = this.postForm;

        for (const field in this.formErrors) {

            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }

        }
    }

    onSubmitValidation() {
        const form = this.postForm;
        for (const field in this.formErrors) {

            this.formErrors[field] = '';
            const control = form.get(field);
            const messages = this.validationMessages[field];

            for (const key in control.errors) {
                this.formErrors[field] += messages[key] + ' ';
            }

        }
    }

    submit() {
        if (this.postForm.valid) {
            this.postData = this.postForm.value;
            this.addNewPost(this.postData);
        } else this.onSubmitValidation()

    }

    addNewPost(data) {
        this.postService.addPost(data).subscribe(() => {
            this.gotoPosts();
            this.toastService.activate('Successfully added');
        },
        () => this.toastService.activate('Error occured'));
    }

    private gotoPosts() {
        this.router.navigate(['/posts']);
    }

}