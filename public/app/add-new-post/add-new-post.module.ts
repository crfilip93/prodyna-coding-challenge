import {
  NgModule
} from '@angular/core';

import {
  AddNewPostRoutingModule,
  routedComponents
} from './add-new-post-routing.module';
import {
  SharedModule
} from '../shared/shared.module';

@NgModule({
  imports: [
    AddNewPostRoutingModule,
    SharedModule,
  ],
  declarations: [routedComponents]
})
export class AddNewPostModule {}