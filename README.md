# Prodyna Coding Challenge

## Introduction

Project consists of node.js using express web server for serving Angular5 application utilising webpack for efficient build and angular-universal which provides server-side-rendering (server side api calls which make prerendered html with working html links allowing webcrawlers to properly index our app) for SEO optimization.

External libraries:
    -css libraries: bootstrap 4, style library for bootstrap 4, font-awesome

My focus wasn't on testing, css and html in this project so I hope you wont mind.

You can check the live version on http://45.33.34.71:4000/

## Installing

    `npm install`

## Deployment

    `npm start`
    port:4000

# Basic structure:

    index.html:
    - basic HTML structure
    - external css libraries
    - app-root of angular app

        app-root:
        - header tag where navbar component is included
        - main tag where router-outlet is included
        - app-footer where component is included
        - app-spinner tag
        - app-modal tag

        router-outlet:
        - home page router link that links to /posts route
        - /posts route
        - /add-new-post route
        - catch-all route which routes to /posts

    core folder:
    - consists of all the core modules and components that angular hierarchical dependency injection tree should have in its root
    - footer is pure html
    - modal: modal which is used in posts as confirmation on delete request
    - navbar: basic navbar
    - spinner: spinner as indicator if the page is loading for any service call
    - animations: animations used in application
    - config: config file for the app
    - exception.service: exception service for api calls
    - module-import-guard: basic precautionary measure for module injection
    - window.service: to not reference window object directly but as a part of app which can be imported when needed

    shared folder:
    - consists of all reusable components and default imports that will be imported by components throughout the app
    - filter-text for filtering array of objects by provided properties
    - init-caps.pipe for making first letter in a sentence upper case

    assets folder:
    - exposed assets used in app
    
    models folder:
    - consists of data models and their api services ( only Post model and service in this case )
    - post.service contains api calls used to populate posts board, add new post and delete existing post 
    
    add-new-post:
    - routable component used for adding new posts

    posts:
    - routable component used for listing posts

    
## Author
    Filip Crncevic
